﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABTravels.Models
{
    public class Ticket:Booking
    {
        public decimal Fare { get; set; }
        public string Airline { get; set; }
        public int Quantity { get; set; }

    }
}