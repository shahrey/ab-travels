﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABTravels.Models
{
    public class Booking
    {
        public string fromPlace { get; set; }
        public string toPlace { get; set; }
        public DateTime departDate { get; set; }
        public DateTime returnDate { get; set; }

    }
}