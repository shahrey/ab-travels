﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ABTravels.Startup))]
namespace ABTravels
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
